% Compute direct dynamic model from Symoro file output

function [ model ] = modelFromSymoro(varargin)
    filename = varargin{1};
    default_pattern = '[A-Za-z]';
    if length(varargin) > 1 && ~isempty(varargin{2})
        n_patterns = varargin{2};
    else
        n_patterns = [];
        patterns{1} = struct('pattern', default_pattern, ...
                             'found', 0, ...
                             'ismatrix', 0, ...
                             'size', [1,1]);
    end
    
    file = fopen(filename, 'r');
    line = fgets(file);
    model = [];
    
    for i=1:length(n_patterns)
        patterns{i} = struct('pattern', n_patterns{i}, ...
                             'found', 0, ...
                             'ismatrix', 0, ...
                             'size', [1,1]);
    end

    % read the whole symoro file
    while ~feof(file)
        passed = false;
        
        % passed becomes true when all the symbolic variables of the
        % evaluated expression have been declared
        while ~passed
            try
                % if the line contains '=' sign, parse the line
                if ~isempty(strfind(line, '='))
                    line = strrep(line, '**', '^');  % replace ** with MATLAB compatible ^
                    eval(line);
                    
                    % process line
                    res = strsplit(line, ' ');       % split expression to retrieve lhs
                    lhs = res{1};
                    
                    for i=1:length(patterns)
                        names = regexp(lhs, ['^(?<var_name>', patterns{i}.pattern,')+(?<digits>\d*)$'], 'names');
                        if ~isempty(names)
                            if isequal(patterns{i}.pattern, default_pattern)
                                patterns{i+1} = patterns{i};
                                patterns{i} = struct('pattern', names.var_name, ...
                                                    'found', 1, ...
                                                    'ismatrix', 0, ...
                                                    'size', [1,1]);
                            end
                            
                            patterns{i}.found = patterns{i}.found + 1;
                            
                            size = patterns{i}.size;
                            d1 = str2num(names.digits(1));
                            
                            if length(names.digits) == 2
                                patterns{i}.ismatrix = 1;
                                d2 = str2num(names.digits(2));
                            elseif length(names.digits) == 1
                                patterns{i}.ismatrix = 0;
                                d2 = 1;
                            end
                            
                            if size(1) < d1
                                patterns{i}.size(1) = d1;
                            end
                            if size(2) < d2
                                patterns{i}.size(2) = d2;
                            end
                        end
                    end
                end
                passed = true;
            catch ME
                switch ME.identifier
                    case 'MATLAB:UndefinedFunction'
                        C = strsplit(ME.message, '''');
                        eval(['syms ', C{2}]);

                    otherwise
                        disp('Unexpected exception :');
                        disp(ME.identifier);
                        disp(ME.message);
                        disp(ME.stack);
                        disp(ME.cause);
                        rethrow(ME);
                end
            end
        end

        line = fgets(file);
    end
    fclose(file);
    
    % eval symoro expressions
    for k=1:length(patterns)
        if patterns{k}.found == 0
            warning(['Pattern ', patterns{k}.pattern, ' not found in this file']');
        else
            expressions = sym(zeros(patterns{k}.size(1), patterns{k}.size(2)));
            
            for i=1:patterns{k}.size(1)
                for j=1:patterns{k}.size(2)
                    if patterns{k}.ismatrix
                        var_name = [patterns{k}.pattern, num2str(i), num2str(j)];
                    else
                        var_name = [patterns{k}.pattern, num2str(i)];
                    end
                    try
                        expressions(i,j) = eval(var_name);
                    catch ME
                        warning(['Evaluating ', var_name, ' returned an error']);
                    end
                end
            end
            model.(patterns{k}.pattern) = struct('expr', expressions, ...
                                                 'size', patterns{k}.size);
        end
    end 
end
