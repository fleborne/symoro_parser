function functionsFromModel(model)
    fields = fieldnames(model);
    for k=1:length(fields)
        filename = [fields{k}, '.m'];
        file = fopen(filename, 'w');
        
        vars = symvar(model.(fields{k}).expr);
        
        fprintf(file, ['function out=', fields{k}, '(args)\n']);
        fprintf(file, '%s\n', ['% required input arguments are:', char(vars)]);
        
        for i=1:model.(fields{k}).size(1)
            for j=1:model.(fields{k}).size(2)
                fprintf(file, ['  out(', num2str(i), ',', num2str(j), ')=', char(model.(fields{k}).expr(i,j)), ';\n']);
            end
        end
        
        fprintf(file, 'end\n');
        fclose(file);
    end
end