function model = symoroNamesReplace(varargin)
    old_names = [];
    new_names = [];
    
    old_str = [];
    new_str = [];
    
    model = varargin{1};
    
    % create old/new names map
    if length(varargin) == 3
        old_str = unique(varargin{2}, 'stable');
        new_str = unique(varargin{3}, 'stable');
    
        if length(old_str) ~= length(new_str)
            error('Old names and new names have different length');
        else
            for i=1:length(old_str)
                old_names = [old_names, sym(old_str{i})];
                new_names = [new_names, sym(new_str{i})];
            end
        end
    end
    
    fields = fieldnames(model);
    vars = [];
    
    % list all the symbolic variables used in the model
    % iterate on model's vectors
    for k=1:length(fields)
        %iterate on matrix fields
        for i=1:model.(fields{k}).size(1)
            for j=1:model.(fields{k}).size(2)
                vars = [ vars, symvar(model.(fields{k}).expr(i,j))];
            end
        end
    end
    
    vars = unique(vars, 'stable');
    new_vars = vars;
    
    %iterate on all the symbolic variables
    for i=1:length(vars)
        parts = regexp(char(vars(i)), '(?<chars>[A-Za-z]+)(?<digits>\d?)', 'names');
        
        if length(parts) == 1
            new_name = parts.chars; % in case we don't want to change this variable name
            
            for k=1:length(old_str)
                if isequal(new_name, old_str{k}) % if we want to change this variable name
                    new_name = new_str{k};
                end
            end
            
            if ~isempty(parts.digits)
                new_vars(i) = sym([new_name, '(', parts.digits, ')']);
            else
                new_vars(i) = sym(new_name);
            end
        else
            error(['Unexpected format for variable ', char(vars(i))]);
        end
    end

    for i=1:length(fields)
        vect = model.(fields{i}).expr;
        new_vect = subs(vect, [old_names, vars], [new_names, new_vars]);
        
        model.(fields{i}).expr = new_vect;
        
        for k=1:length(old_str)
            if isequal(fields{i}, old_str{k}) % if we want to change this variable name
                new_field = new_str{k};
                model.(new_field) = model.(fields{i});
                model = rmfield(model, fields{i});
            end
        end
    end
    
end