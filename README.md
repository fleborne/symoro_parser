# MATLAB-OpenSymoro interface #


## modelFromSymoro ##

Converts a Symoro file into a set of MATLAB expressions.

Usage:
```matlab
model = modelFromSymoro(filename, patterns)
```

### Input arguments ###
* **filename** (string): path to the Symoro file containing the requested model.
* **patterns** (cell-array of strings): root of Symoro variables names to include in the model. It is possible to use names having more than one character. If no patterns is specifies, `modelFromSymoro` will try to match as many fields as possible.

### Output argument ###

**model** (struct): `model` is a structure whose fields depend on the input argument `patterns`. Each field has two subfields. `expr` stores the symbolic expression of the corresponding variable. `size` stores the dimension of the variable.

### Example ###
With the following parameters, `model` has two fields (C and V) and field C has these two subfields:
```matlab
>> filename = '/home/francois/titan2_vel.txt';
>> patterns = {'C', 'V'};

>> model = modelFromSymoro(filename, patterns);

model.C.expr:
 cos(q(1))
 cos(q(2))
 cos(q(3))

model.C.size:
     3     1
```


## symoroNamesReplace ##

Groups variables from a same matrix (resp. vector) in a single symbolic matrix (resp. vector). It is also possible to replace the names of some variables.

Usage:

```matlab
model = symoroNamesReplace(original_model, old_names, new_names)
```

### Input arguments ###

* **original_model** (struct): the output of `modelFromSymoro`
* **old_names** (cell-array of strings, *optional*): original names found in Symoro output
* **new_names** (cell-array of strings, *optional*): new names to be substituted to the original names

### Output argument ###

**model** (struct): the MATLAB model with new names and grouped expressions

### Example ###
```matlab
>> model = symoroNamesReplace(model, {'GAM', 'GZ'}, {'tau', 'g'});
```

`GAM1`, `GAM2`, and `GAM3` are grouped in a single `tau` vector having three cells, and `GZ` is replaced with `g`.


## functionsFromModel ##

Creates MATLAB function files from all the fields found in `model`.

Usage:
```matlab
functionsFromModel(model)
```

### Input argument ###

* **model** (struct): the output of `modelFromSymoro` or `symoroNamesReplace`

### Output argument ###

* no output arguments, but creates one function file for each field of `model`
