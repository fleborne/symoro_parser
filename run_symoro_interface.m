% get model from Symoro file
symoro_model = '/home/francois/gdrive/these/data/symoro-robots/arm5_3axis/arm5_3axis_base_ddm.txt';

patterns = {'QDP'};

model = modelFromSymoro(symoro_model, patterns);

% replace variable names
old_names = {'th', 'QP', 'QDP', 'GAM', 'GZ'};
new_names = {'q',  'qd', 'qdd', 'current', 'g'};
model_new_names = symoroNamesReplace(model, old_names, new_names);

% write model in function files
dir='generated';
mkdir(dir); cd(dir);

functionsFromModel(model_new_names);

cd('..');